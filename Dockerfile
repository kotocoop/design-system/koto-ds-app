FROM docker.io/node:22.11@sha256:5c76d05034644fa8ecc9c2aa84e0a83cd981d0ef13af5455b87b9adf5b216561

ENV PATH "$PATH:./node_modules/.bin"
EXPOSE 8008
EXPOSE 14881

WORKDIR /code/app

RUN apt -y install python3

#RUN curl -L https://raw.githubusercontent.com/npm/self-installer/master/install.js | node

RUN echo update 20200408_1100

RUN npm i -g pnpm

RUN pnpm install types @types/express express body-parser serve-static

RUN pnpm install leveldown
RUN pnpm install ssb-db2
RUN pnpm install ssb-feed ssb-keys
RUN pnpm install typescript leveldown
RUN pnpm install ssb-links
RUN pnpm install blake3@2.1.7
RUN pnpm install ssb-db2 ssb-ebt ssb-friends multiformats secret-stack ipfs-http-client

ADD app/package.json package.json
RUN pnpm install

RUN rm node_modules/koto* || true

ADD tmp-pack/kotods-ssb.tgz node_modules/
RUN mv node_modules/package node_modules/kotods-ssb

ADD tmp-pack/kotods-api.tgz node_modules/
RUN mv node_modules/package node_modules/kotods-api

ADD app/src /code/app/src
#ADD app/*.ts /code/app/
ADD app/*.json /code/app/

ADD app/test/data /code/app/test/data

RUN ./node_modules/.bin/tsc

RUN find /code/app/ | grep -v node_mod 

ENTRYPOINT node /code/app/src/run.js
