#!/bin/bash

set -e 

c=$(pwd)/$(dirname $0)
echo current_dir $c

export PATH=$PATH:./node_modules/.bin

cd ${c}/app

if [ ! -d node_modules ]; then
	pnpm install
fi

if tsc; then
	#echo TRYING TO RUN
	#node build/run.js
	
	cd ${c}

	export TEST_DATA_PATH="${c}/app/test/data"
	IPFS_API_URL="http://0.0.0.0:5001/api/v0" node app/test/localtests.js

	rm -r ${c}/tmp-pack || true
	mkdir -p ${c}/tmp-pack/
	sh -c "cd app/node_modules/kotods-ssb && npm pack --pack-destination ${c}/tmp-pack"
	sh -c "cd app/node_modules/kotods-api && npm pack --pack-destination ${c}/tmp-pack"
	
	for file in ${c}/tmp-pack/*.tgz; do
		echo FILE ${file}
		
	    new_name=$(echo "$file" | sed -r 's/-[0-9]+\.[0-9]+\.[0-9]+\.tgz$/.tgz/')
	    if [ "$new_name" != "$file" ]; then
	        echo "Renaming $file to $new_name"
	        mv "$file" "$new_name"
	    fi
	done
fi
