#!/bin/bash

echo RUN SERVERS

export PATH=./node_modules/.bin:$PATH

ps aux | grep "bash runserver.sh" |  awk '{ print $2; }' | xargs -l1 kill

rm info/0* || true

bash runserver.sh "001" & 
bash runserver.sh "002" &

while [ ! -f info/002.status ]; do
	echo "Waiting for status in $(pwd)/info/002.status"
	sleep 2
done

echo RUNSERVERS DONE

sleep 1
