#!/bin/bash

number=$1
contname=testenv-ssb$1-1

echo RUN SERVER ${number} ${contname}

api () {
    #echo docker exec -it ${contname} sh -c "ssb-server $*"
    #docker-compose exec ${contname} sh -c "export; file ./node_modules/.bin/ssb-server; which ssb-server"    
    docker exec ${contname} sh -c "curl http://localhost:14881/$*" 
}

echo using contname ${contname}

export HOME=$home
export ssb_appname="ct-test" 

sleep 2

while  [ -z "$(api "info" | jq .userid)" ]; do
	echo "waiting for ${number}"
	if [ -z "$(docker ps | grep ${contname})" ]; then
		echo ${contname} not found!
		exit 1
	fi
	
	sleep 2
done

echo writing info ${contname} to $(pwd)/info/$1.whoami

api "info" | jq -r .userid > info/$1.whoami
api "info" | jq -r .address > info/$1.address
api "info" > info/$1.status

api users/peers

echo RUNSERVER $contname DONE
