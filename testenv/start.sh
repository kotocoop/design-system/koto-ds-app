#!/bin/bash

cd $(dirname $0)
pwd

ps aux | grep "tail -f compose.log" |  awk '{ print $2; }' | xargs -l1 kill

killall docker-compose

docker-compose rm -f
#docker-compose build

docker-compose up > compose.log &

sleep 1

tail -f compose.log&


bash runservers.sh 

echo TESTENV STARTED
