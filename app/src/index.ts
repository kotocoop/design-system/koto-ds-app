import * as pluralize from 'pluralize';
import { CTMessageContent, CTApi, CTApps } from "kotods-api";


/**
* @Method: Returns the plural form of any noun.
* @Param {string}
* @Return {string}
*/
export function getPlural(str: any): string {
  return pluralize.plural(str)
}

export { CTApi, CTApps, CTMessageContent }
