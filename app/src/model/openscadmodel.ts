
export default class KDSOpenscadModel {	
	content : string

	setContent(content : string) {
		this.content = content;
	}
	    
	getContent(): string {
		return this.content;
	}
}