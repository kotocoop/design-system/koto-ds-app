import { CTMessageContent, CTApi, CTApps } from "kotods-api";

export default class KDSItem {
	meta: { [ key : string ]: string }
	models: {
		"view": any
	}
	children: any
	
	getViewModel(key : string): any {
		return this.models.view[key];
	}
}