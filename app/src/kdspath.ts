

export default class KDSPath {
	releaselists : string;
	releaselist : string;
	name : string;
	
	constructor(path: string) {
		this.parse(path);
	}
	
	parse(path : string) {
		var splitted = path.split(":");
		this.releaselists = splitted[0];
		this.releaselist = splitted[1];
		this.name = splitted[2];		
	}
}