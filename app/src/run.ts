console.log("Start running CTApp");
import { CTApp } from "kotods-api";
console.log("imported CTApp");
import { CTApi } from "kotods-api";

let ctapi: CTApi;
let ctapp: CTApp = new CTApp();

console.log( "Running CTApp");

ctapp.init().then(() => {
    ctapi = ctapp.getApi();

	
	if (process.env.TEST_LAUNCH) {
		console.log("process.env.TEST_LAUNCH " + process.env.TEST_LAUNCH);
		console.log("Just testing");
		process.exit(0);
	} else {
    	ctapi.start();
    }

    console.log( "CTApp created" );
    //ctapi.stop();
} ).catch(( err: string ) => {
	console.log("Run error")
    if ( err ) {
        console.log( "Run creation error " + err );
        process.exit( 1 );
    }
} );
