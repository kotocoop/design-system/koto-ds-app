import ReleaseList from '../releaselist'
import DictReleaseList from './dictreleaselist.js'

import fs from 'fs';
import path from 'path';

export default class PathReleaseList implements ReleaseList {	
	dictlist : DictReleaseList = new DictReleaseList();	

	path: string
	TYPE: string = "PATHLIST";
	
    constructor( path : string) {
    	this.path = path;
    }
    
    getType(): string {
    	return this.TYPE;
    }
    
    async update() {
    	console.log("PathReleaseList current path " + path.resolve(process.cwd()));
    	
		var ncontent : { [key : string ] : { [key : string ] : string } } = {}
		
    	const pathcontent = await fs.promises.readdir( this.path );
    	for( const file of pathcontent ) {
    		console.log("PathReleaseList " + this.path + " file:" + file);
    		var subpath = path.join(this.path, file);
    		const stat = await fs.promises.stat(subpath);
    		if( stat.isDirectory() ) {
    			ncontent[file] = { }

		    	const files = await fs.promises.readdir( subpath );
		    	for( const listfile of files ) {
		    		console.log("PathReleaseList " + subpath + " file:" + listfile);
		    		ncontent[file][listfile] = "file:" + path.join(subpath, listfile);
		    	}
    			
    		}
    	}
    	
    	this.dictlist.content = ncontent;
    	
		console.log("PAthReleaseList updated from " + this.path + " content:" + JSON.stringify(this.getContent()));
    }
    
	getContent(): { [key : string]: { [key : string] : string }} {				
		return this.dictlist.content;
	}
	
	hasValue(groupname : string, valuename : string) : boolean {
		return this.dictlist.hasValue(groupname, valuename);
	}
	
	getValue(groupname : string, valuename : string) : string {
		return this.dictlist.getValue(groupname, valuename);	
	}
}
