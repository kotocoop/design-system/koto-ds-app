import ReleaseList from '../releaselist'
import DictReleaseList from './dictreleaselist.js'

import bent from 'bent';
const getJSON = bent('json')


export default class UrlReleaseList implements ReleaseList {
	dictlist : DictReleaseList = new DictReleaseList();
	url : string
	TYPE: string = "URLLIST";
	
    constructor( url : string) {
    	this.url = url;
    }
    
    getType(): string {
    	return this.TYPE;
    }
    
    async update() {
    	this.dictlist.content = await getJSON(this.url);
		console.log("UrlReleaseList updated from " + this.url + " content:" + JSON.stringify(this.getContent()));
    }
    
	getContent(): { [key : string]: { [key : string] : string }} {				
		return this.dictlist.content;
	}
	
	hasValue(groupname : string, valuename : string) : boolean {
		return this.dictlist.hasValue(groupname, valuename);
	}
	
	getValue(groupname : string, valuename : string) : string {
		return this.dictlist.getValue(groupname, valuename);	
	}

}
