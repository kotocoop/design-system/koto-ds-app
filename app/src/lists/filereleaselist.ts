import * as fs from 'fs';
import ReleaseList from '../releaselist'
import DictReleaseList from './dictreleaselist.js'

export default class FileReleaseList implements ReleaseList {	
	dictlist : DictReleaseList = new DictReleaseList();	

	filepath : string
	TYPE: string = "FILELIST";
	
    constructor( filepath : string) {
    	this.filepath = filepath;
    	this.update();
    }

    getType(): string {
    	return this.TYPE;
    }
    
    async update() {
		const jsonData = fs.readFileSync(this.filepath, 'utf-8');
		this.dictlist.content = JSON.parse(jsonData);
		console.log("FileReleaseList updated from " + this.filepath + " content:" + JSON.stringify(this.getContent()));
    }
    
    store() {
    	fs.writeFileSync(this.filepath, JSON.stringify(this.getContent(), null, 2));
    }
    
	getContent(): { [key : string]: { [key : string] : string }} {				
		return this.dictlist.content;
	}
	
	hasValue(groupname : string, valuename : string) : boolean {
		return this.dictlist.hasValue(groupname, valuename);
	}
	
	getValue(groupname : string, valuename : string) : string {
		return this.dictlist.getValue(groupname, valuename);	
	}
}