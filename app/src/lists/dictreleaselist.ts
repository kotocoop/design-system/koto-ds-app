import ReleaseList from '../releaselist'

export default class DictReleaseList implements ReleaseList {
	content : { [key : string ]: { [ key : string ] : string } } = {};

	getContent(): { [key : string]: { [key : string] : string }} {				
		return this.content;
	}
	
    getType(): string {
		console.log("ERROR DictReleaseList getType shouldn't be called");
    	return "ERROR";
    }
    	
	async update() {
		console.log("ERROR DictReleaseList update shouldn't be called");
	}
	
	hasValue(groupname : string, valuename : string) : boolean {
		return (this.content.hasOwnProperty(groupname) && this.content[groupname].hasOwnProperty(valuename) );
	}
	
	getValue(groupname : string, valuename : string) : string {
		if(this.content.hasOwnProperty(groupname) && this.content[groupname].hasOwnProperty(valuename) ) {
			return this.content[groupname][valuename];
		}
		
		return "unknown";
	}

}