/*
    Copyright (C) 2023  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { plainToClass } from 'class-transformer';

import KDSItem from './model/item.js'
import KDSPath from './kdspath.js'
import KDSOpenscadModel from './model/openscadmodel.js'
import KotoReleaseLists from './kotoreleaselists.js'

import bent from 'bent'
const getJSON = bent('json')

import * as fs from 'fs';

import child_process from 'child_process';

export default class KDSLoader {
	storageapiurl : string
	releaselists : KotoReleaseLists;
	
	constructor(storageapiurl: string, releaselists : KotoReleaseLists) {
		this.storageapiurl = storageapiurl;
		this.releaselists = releaselists;
	}

	async loadItemWithFilePath(filepath : string): Promise<KDSItem> {
		const filedata = fs.readFileSync(filepath, 'utf-8');
		let items = plainToClass(KDSItem, JSON.parse(filedata));
		console.log("items : " + JSON.stringify(items));
		
		return items;
	}

	async loadOpenscadModelWithFilePath(filepath : string): Promise<KDSOpenscadModel> {
		var m : KDSOpenscadModel = new KDSOpenscadModel()
		m.setContent(this.readFile(filepath));
		return m;
	}
	
	async loadOpenscadModelWithPath(spath : string): Promise<KDSOpenscadModel> {
		console.log("loadOpenscadModelWithPath " + spath);

		var path : KDSPath = new KDSPath(spath);
		var value : string = this.releaselists.getValue(path);
		
		console.log("loadOpenscadModelWithPath path: " + path + " got value: \"" + value + "\"");
				
		if (value.startsWith("file:")) {
			var filepath : string = value.split(":")[1];
			return await this.loadOpenscadModelWithFilePath(filepath);	
		} else {
			var jsondata = await getJSON(this.storageapiurl + "/storage/get/" + value);
			
			var m : KDSOpenscadModel = new KDSOpenscadModel();
			m.setContent(jsondata);
			return m;
		}
	}
	
	async storeItem(item: KDSItem): Promise<string> {
		var viewmodel : any = item.getViewModel("default");
		
		// TODO how to calculate content hash and when. Is this needed?
		viewmodel.hash = viewmodel.ID
		 
		if (viewmodel.hash != viewmodel.export.modelhash) {
			console.log("storeItem : should export");
			
			var openscadmodel : KDSOpenscadModel = await this.loadOpenscadModelWithPath(viewmodel["release-path"]);
			viewmodel.ID = await this.store(openscadmodel.getContent());

			var tmpfile : string = "tmp/" + viewmodel.ID + ".scad" 
			fs.writeFileSync(tmpfile, openscadmodel.getContent());

			var stlfile : string = "tmp/output-" + viewmodel.ID + ".stl"
			var pngfile : string = "tmp/output-" + viewmodel.ID + ".png"
			
  			child_process.execSync("openscad -o " + stlfile + " " + tmpfile, { cwd: '.', stdio: 'inherit' });
  			child_process.execSync("openscad -o " + pngfile + " " + tmpfile, { cwd: '.', stdio: 'inherit' });
  			
  			viewmodel.export.type = "STL";
  			viewmodel.export.ID = await this.storeFile(stlfile);
  			viewmodel.image = await this.storeFile(pngfile);
  			
			viewmodel.export.modelhash = viewmodel.hash
		}
		
		return this.store(item);
	}

	async loadItemWithPath(path : string): Promise<KDSItem> {
		console.log("loadItemWithPath " + path);
		var kdspath : KDSPath = new KDSPath(path);
		var list : any = this.releaselists.getList(kdspath.releaselists).getContent()
		
		console.log("loadItemWithPath list:" + JSON.stringify(list));
		console.log("loadItemWithPath releaselist:" + kdspath.releaselist + " name:" + kdspath.name + " list content:" + JSON.stringify(list[kdspath.releaselist]));
				
		var itemid : string = "" + list[kdspath.releaselist][kdspath.name];
		console.log("loadItemWithPath itemid: " + itemid);
		var jsondata = await getJSON(this.storageapiurl + "/storage/get/" + itemid);
		console.log("loadItemWithPath jsondata " + JSON.stringify(jsondata));

		var data = Buffer.from(jsondata.content, 'base64').toString('utf-8');
					
		return this.getItemWithJSON(JSON.parse(data));
	}
	
	getItemWithJSON(jsondata : string) : KDSItem {
		let items = plainToClass(KDSItem, jsondata);
		console.log("items : " + JSON.stringify(items));
		return items;			
	}
	
	async storeFile(filepath : string): Promise<string> {
    	console.log("adding to storage file:" + filepath);	
		var buffer : any = this.readBinaryFile(filepath);
		var base64content : string = buffer.toString('base64')
		
		return this.store64content(base64content);
	}
	
    async store(content : any): Promise<any> {
    	if (typeof content === 'string' || content instanceof String) {
    		content = "" + content
    	} else {    	
			content = this.json(content);
		}
		
    	console.log("adding to storage " + content);
    	
    	var base64content : string = Buffer.from(content).toString('base64')
    	return this.store64content(base64content);
    }
    
    async store64content(base64content : any) : Promise<any> {
    	var postcontent = {
    		"data": base64content
    	}

    	//console.log("posting " + JSON.stringify(postcontent));
    	
        const post = bent(this.storageapiurl, 'POST', 'json', 200);
        var result : any = await post('/storage/add', postcontent);
    	return result.id;
    }
    
    readBinaryFile(filepath : string) : any {
		return fs.readFileSync(filepath);     
    }
    
	readFile(filepath : string) : any {
		return fs.readFileSync(filepath, 'utf-8');
	}
		
	json(content : any) : string {
		return JSON.stringify(content, null, 2);	
	}
	
}	