import UrlReleaseList from './lists/urlreleaselist.js';
import PathReleaseList from './lists/pathreleaselist.js';
import ReleaseList from './releaselist.js';
import KDSPath from './kdspath.js'

export default class KotoReleaseLists {
	lists : { [key : string] : ReleaseList	}
	
	constructor() {
		this.lists = {
			'koto-ci-test': new UrlReleaseList("https://kotocoop.gitlab.io/design-system/kotods-public-lists/release-lists.json"),
 			'test': new PathReleaseList("app/test/data")
		}
	}

	async update() {
		for (const [key, value] of Object.entries(this.lists)) {
			await value.update();
		}
	}
	
	getList(name : string): ReleaseList {
		console.log("KotoReleaseLists getList " + name);
		return this.lists[name];
	}
	
	getValue(path : KDSPath): string {
		if(path.releaselists == "any") {
			for (const [key, list] of Object.entries(this.lists)) {
				if(list.hasValue(path.releaselist, path.name)) {
					return list.getValue(path.releaselist, path.name);
				}
			}		
		} else {
			var list : ReleaseList = this.lists[path.releaselists];
			return list.getContent()[path.releaselist][path.name];
		}
		
		return "unknown";
	}
}