
import KDSLoader from './loader.js'
import KDSItem from './model/item.js'
import KDSOpenscadModel from './model/openscadmodel.js'
import KotoReleaseLists from './kotoreleaselists.js'

export default class KDSHandler {
	loader : KDSLoader;
	releaselists : KotoReleaseLists;
	
	constructor(loader : KDSLoader, releaselists : KotoReleaseLists ) {
		this.loader = loader;
		this.releaselists = releaselists;
    }
	

	async loadItem(basepath : string, path : string) : Promise<KDSItem> {
		var item1 : KDSItem = await this.loader.loadItemWithFilePath(basepath + path);
		if (item1.models && item1.models.view && 'default' in item1.models.view) {
			var itempath : string = basepath + item1.models.view['default']['release-path'];
			var openscad1 : KDSOpenscadModel = await this.loader.loadOpenscadModelWithFilePath(itempath);
			var item1scadid : string = await this.loader.store(openscad1.getContent());
			item1.getViewModel("default")['ID'] =  item1scadid;
		}
		
		return item1;		
	}
}

