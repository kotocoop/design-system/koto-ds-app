export default interface ReleaseList {
	update(): Promise<void>;
	getContent(): { [key : string]: { [key : string] : string }};	
	getType(): string;
	hasValue(group : string, valuename : string): boolean;
	getValue(group : string, valuename : string): string;
}