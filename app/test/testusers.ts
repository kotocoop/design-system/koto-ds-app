/*
    Copyright (C) 2017  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { strict as assert } from 'node:assert';

import { Message, TestMessages } from './messages';

var messages = new TestMessages();

import bent from 'bent'
const getJSON = bent('json')

export default class UsersTests {
    urls: string[];

    constructor(nurls : string[]) {
    	this.urls = nurls
    }

    async run() {
        var url1 = this.urls[0]
        var url2 = this.urls[1]

        const post1 = bent(url1, 'POST', 'json', 200);
		
		var waitok = await getJSON(url1 + "/wait")
		console.log("get lists1 " + JSON.stringify(waitok));

		var waitok = await getJSON(url2 + "/wait")
		console.log("get lists1 " + JSON.stringify(waitok));

//        await this.users.waitIfEmpty();
//        var userlist: {} = await this.users.getFollowing();
//        assert.equal( true, Object.keys( userlist ).length > 0 );
    }
}
