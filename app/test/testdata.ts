/*
    Copyright (C) 2023 Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { strict as assert } from 'node:assert';

import FileReleaseList from '../src/lists/filereleaselist.js'
import KDSLoader from '../src/loader.js'
import KDSItem from '../src/model/item.js'
import KDSOpenscadModel from '../src/model/openscadmodel.js'
import KotoReleaseLists from '../src/kotoreleaselists.js'

import bent from 'bent'
const getJSON = bent('json')
const getBuffer = bent('buffer')

export default class DataTests {
	loader : KDSLoader;
	users : any
    releaselists : KotoReleaseLists;

    constructor( nusers : any, releaselists : KotoReleaseLists ) {
		this.users = nusers
		this.releaselists = releaselists;
    }
	
	async run() {
		try {
			await this.releaselists.update();

			var basepath : string = "" + process.env.TEST_DATA_PATH

			var list = new FileReleaseList(basepath + "/release-lists.json");
			console.log("list " + JSON.stringify(list.getContent()));
			var content : any = list.getContent();
			var releases = content['kotocoop-test'];
			
			console.log("TESTDATA users " + JSON.stringify(this.users));
			
			console.log("API url " + this.users[0].url);
			
			const getStream = bent(this.users[0].url)		
			this.loader = new KDSLoader(this.users[0].url, this.releaselists);
			
			var stream : any = await getStream("/storage/get/Qmc3zqKcwzbbvw3MQm3hXdg8BQoFjGdZiGdAfXAyAGGdLi")
			var test64 = "" + JSON.parse(await stream.text()).content;
			var testtxt = Buffer.from(test64, 'base64').toString('utf-8');
			assert.equal(testtxt, "Don't we all.");

			var item1 : KDSItem = await this.loadItem(basepath, "/test/item1.json");
			var item2 : KDSItem = await this.loadItem(basepath, "/test/item2.json");
			
			var parentitem : KDSItem = await this.loadItem(basepath, "/test/parent-object.json");
			parentitem.children['item1']['ID'] = await this.loader.storeItem(item1);
			parentitem.children['item2']['ID'] = await this.loader.storeItem(item2);

			var parentid : any = await this.loader.store(parentitem);
			console.log("parent item : " + JSON.stringify(parentitem));
			console.log("parent item ID : " + JSON.stringify(parentid));
			
			releases["parent1"] = parentid;
			console.log("list " + JSON.stringify(list.getContent()));
			list.store();

			var parentitem2 : KDSItem = await this.loader.loadItemWithPath("koto-ci-test:kotocoop-test:parent1");
			assert.equal(parentitem.meta['name'],parentitem2.meta['name']); 
			assert.equal(this.loader.json(parentitem),this.loader.json(parentitem2));  
		} catch(e) {
			console.log("ERROR " + e);
			process.exit(1);
		}
		
		return "DONE";
	}
	
	async loadItem(basepath : string, path : string) : Promise<KDSItem> {
		var item1 : KDSItem = await this.loader.loadItemWithFilePath(basepath + path);
		if (item1.models && item1.models.view && 'default' in item1.models.view) {
			var itempath : string = item1.models.view['default']['release-path'];
			console.log("loadItem itempath : " + itempath);
			
			var openscad1 : KDSOpenscadModel = await this.loader.loadOpenscadModelWithPath(itempath);
			var item1scadid : string = await this.loader.store(openscad1.getContent());
			item1.getViewModel("default")['ID'] =  item1scadid;
		}
		
		return item1;		
	}
}

