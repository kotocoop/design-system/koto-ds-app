/*
    Copyright (C) 2017  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { strict as assert } from 'node:assert';
import { Message, TestMessages } from './messages.js';

import bent from 'bent'
const getJSON = bent('json')

export default class StorageTests {
	users : any
	
    constructor( nusers : any ) {
		this.users = nusers
    }

    async run() {		
        var url = this.users[0].url
        const post = bent(url, 'POST', 'json', 200);
    
        console.log("writing to storage")
        var testdata : string = "koto-testdata"
        
		const response = await post('/storage/add', { testpath: 'teststorage', data: testdata });

		assert.equal(JSON.stringify(response), "{\"id\":\"QmfQCD9zon2iyNNtqYGerF4kNgReYwjLrCVkPiwxAC2mCP\"}");
    }
}