/*
    Copyright (C) 2018  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { strict as assert } from 'node:assert';

import BasicTests from './testbasic.js';
import ListTests from './testlists.js';
import UsersTests from './testusers.js';
import DataTests from './testdata.js';
import StorageTests from './teststorage.js';
import KotoReleaseLists from '../src/kotoreleaselists.js'

import bent from 'bent'
const getJSON = bent('json')

var config = {};

interface UserInfo {
    userid: string
}

var userone: UserInfo;
var usertwo: UserInfo;

async function runtests() {
	console.log("ASYNC");

	var users = [
		{ "url" : "http://" + process.env.TEST_HOSTPORT1 || "http://localhost:14001", "info": {} }, 
		{ "url" : "http://" + process.env.TEST_HOSTPORT2 || "http://localhost:14002", "info": {} },
		{ "url" : "http://" + process.env.TEST_HOSTPORT3 || "http://localhost:14003", "info": {} }
	]
	
    var app = null;
	try {		
		console.log("TESTS: LAUNCHING BASIC");
	    var basictests = new BasicTests(users);
	    await basictests.run();

		console.log("TESTS: GETTING USER INFO " + JSON.stringify(users, null, 4));
		
        var userone = await getJSON(users[0].url + "/info");
        users[0].info = userone
        var usertwo = await getJSON(users[1].url + "/info");
		users[1].info = usertwo

        var usersresponse = await getJSON(users[0].url + "/users");
        console.log("users1 " + JSON.stringify(userone) + " users:" + usersresponse);
        
        usersresponse = await getJSON(users[1].url + "/users");
        console.log("users2 " + JSON.stringify(usertwo) + " users:" + usersresponse);
        
        console.log("TESTS userone " + JSON.stringify(userone));
        console.log("TESTS usertwo " + JSON.stringify(usertwo));

        console.log("followed " + JSON.stringify(
        	await getJSON(users[1].url + "/users/follow/" + encodeURIComponent(userone.userid)))
        );
        console.log("followed " + JSON.stringify(
        	await getJSON(users[0].url + "/users/follow/" + encodeURIComponent(usertwo.userid)))
        );

        //console.log("TESTS: LAUNCHING USERS");
        //await new UsersTests(urls).run();
        
        console.log("TESTS: ALL POSTS");
        await getJSON(users[0].url + "/posts/all");
        await getJSON(users[1].url + "/posts/all");
        
        console.log("TESTS: LAUNCHING STORAGE");
        await new StorageTests(users).run();
        
        console.log("TESTS: LAUNCHING LISTS");
        await new ListTests(users).run();
        
        await new DataTests(users, new KotoReleaseLists()).run();        
	} catch(e) {
        console.log("END TESTS " + e);
//        if(app) {
//            app.stop();
//        }
	} finally {	
        console.log("END");
	}

	console.log("ASYNC END");
	
	return "DONE";
}

runtests().then((response : any) => {
	process.exit(0);
});

console.log("TESTS sync END");
