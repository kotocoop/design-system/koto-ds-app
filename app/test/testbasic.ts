/*
    Copyright (C) 2018  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { strict as assert } from 'node:assert';

import { CTMessageContent } from 'kotods-api';
import { Message, TestMessages } from './messages.js';

import bent from 'bent'
const getJSON = bent('json')

var messages = new TestMessages();

export default class BasicTests {
    users: any;

    constructor(nusers : any) {
    	this.users = nusers
    }

    async run() {
        //assert.equal("hello", this.api.info().hello);
        var self = this;
        
        const post1 = bent(this.users[0].url, 'POST', 'json', 200);
        const post2 = bent(this.users[1].url, 'POST', 'json', 200);
        
        console.log("basictest get users " + this.users[0].url + "/info");
        var info = await getJSON(this.users[0].url + "/info")
        console.log("got info " + JSON.stringify(info));

    	assert.equal(process.env.SSB_TEST001_WHOAMI, info.userid);
        
        console.log("basictest sending message");
        var message = messages.getBasic();
        message.content = "Important message";
        var mc = new CTMessageContent();
        
        console.log("sending " + this.users[0].url)

//        console.log("basictest addMessage");
//        await this.api.addMessage(mc, "test", (err: string, msg: string) => {
//        	assert.ifError(err);
//			console.log("test message added");
//        });

        console.log("basictest end");
	}
}