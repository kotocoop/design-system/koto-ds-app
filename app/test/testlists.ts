/*
    Copyright (C) 2017  Juuso Vilmunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

import { strict as assert } from 'node:assert';

import { Message, TestMessages } from './messages.js';

import bent from 'bent'
const getJSON = bent('json')

var messages = new TestMessages();

export default class ListTests {
	users : any
	
    constructor( nusers : any ) {
		this.users = nusers
    }

    async run() {
        var url = this.users[0].url
        const post = bent(url, 'POST', 'json', 200);
		
        console.log("writing to list")
        var testdata : string = "testdata" + Date.now()
        
		const response = await post('/lists/write', { path: 'testlist', data: testdata });
		console.log("response " + response)
//
		var lists1 = await getJSON(this.users[0].url + "/lists")
		console.log("get lists1 " + JSON.stringify(lists1));

		while(true) {
			await new Promise( resolve => setTimeout( resolve, 5000 ) );
			
			var lists2 = await getJSON(this.users[1].url + "/lists")
			console.log("get lists2 " + JSON.stringify(lists2, null, 4));
			
			if (Object.keys(lists2).length > 0) {
				break;
			}
		}
		
		var lists1 = await getJSON(this.users[0].url + "/lists")		
		assert.equal(JSON.stringify(lists1), JSON.stringify(lists2));
		assert.equal( lists2[this.users[0].info.userid]["/testlist"], testdata );
    }
}