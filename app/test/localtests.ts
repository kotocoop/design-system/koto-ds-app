
import { CTStorageApi, CTApi, CTApp } from 'kotods-api'
import DataTests from './testdata.js'
import KotoReleaseLists from '../src/kotoreleaselists.js'

var ipfs_api_url = ""

var app = new CTApp(); 
await app.init();
app.getApi().start();

var users = [
	{ "url" : "http://localhost:14881", "info": {} } 
]


await new DataTests(users, new KotoReleaseLists()).run();
process.exit(0);

