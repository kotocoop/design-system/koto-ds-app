#!/bin/bash

set -e 

c=$(pwd)/$(dirname $0)
echo current_dir $c

export PATH=$PATH:./node_modules/.bin

export TEST_DATA_PATH="${c}/app/test/data"

if bash prepare-build.sh; then

    DOCKER_BUILDKIT=1 BUILDKIT_PROGRESS=plain docker build . -t ct-api-base
	docker run --rm -e TEST_LAUNCH=true --name kotods-test ct-api-base
	
	bash testenv/start.sh
	
	export ssb_appname=ssb-test
	export HOME=$(pwd)/testenv/users/001 
	echo HOME ${HOME}

	export TEST_HOSTPORT1=localhost:14001
	export TEST_HOSTPORT2=localhost:14002
	export TEST_HOSTPORT3=localhost:14003
	
	. $c/testenv/test_values.sh

	echo RUNNING TESTS
	node --unhandled-rejections=strict app/test/tests.js
	#npm run test
	cd ${c}
	
	echo tests done. 
	if [ -z "${NO_STOP}" ]; then
		echo calling stop.
		bash testenv/stop.sh
	fi
else
	bash testenv/stop.sh
	exit 1;
fi

echo DONE
